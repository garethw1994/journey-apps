## Journey Apps

### Prerequisites
- NPM >= 5.6
- Node >= 8.10

### Getting Started
- Clone this repository by clicking the clone button in the top right corner and copy the clone command.
- Alternatively you can run 
    `` git clone git@bitbucket.org:garethw1994/journey-apps.git `` 
    or 
    `` git clone https://garethw1994@bitbucket.org/garethw1994/journey-apps.git ``
    in your command line.
- Change into the ``` JourneyApps ``` directory.
- Next install dependencies by running the following command in the root of the folder:
    `` npm install ``

### Running App
- After the dependency installation is complete make sure to run the app over HTTPS by running the following command:
    `` HTTPS=true npm start ``
- If you get a security/privacy warning while loading the app, just click the advanced button and ``` Proceed to localhost (unsafe) ``` link. 