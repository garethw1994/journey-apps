/**
    * DataTransformation() is a method to take current list items 
    * and transform it to required output for Inventory Management System
        * @argument {lineItems} 
            * all line items from backend-service    
        * @returns {newLineItems} 
*/
const DataTransformation = async (lineItems) => {
    // all items from backend-service
    let items = lineItems["line_items"];

    // build unique document id list
    let initialData = await BuildPO(items);

    // populate document id list or PO list with line item data
    let newLineItems = await buildLineItemData(items, initialData);

    // return final mapped items
    return newLineItems;
};

/**
    * BuildPO() is a method to take current list items 
    * and builds up a key value pair object with the documentid props
        * @argument {lineData} 
            * all line items   
        * @returns {initialData} 
*/
const BuildPO = (lineData) => {
    let initialData = {};
    // iterate over list items 
    // map key, value pair object where the value is empty array
    lineData.map((line) => {
        let documentId = line.documentid;
        if (initialData[documentId] === undefined) {
            initialData[documentId] = [];
            return true;
        };
        return false;
    });
    // return final data
    return initialData;
};

/**
    * buildLineItemData() is a method to take current list items and a list of PO items 
    * to populate each PO with a line item and lot numbers
        * @argument {lineData} 
            * all line items
        * @argument {initialData} 
            * list of PO date that contains only documentids mapped to empty array
        * @returns {newLineItems} 
*/
const buildLineItemData = (lineData, initialData) => {
    // PO id mapped to empty array object
    let initData = initialData;

    // Store Previous Line Item ID
    let previousLineItem = "";

    // iterate over all list items
    lineData.map((item) => {
        // curent document id
        let documentId = item.documentid;

        // Map Line Item Data
        let lineItemData = {
            stockCode : item.item_code,
            lineItemId : item.unique_number,
            packSize : item.pack_size,
            quantity : item.quantity,
            lotNumbers: []
        };

        // Map Lot Number Data
        let lotNumber = {
            lotNumber: item.batch_number,
            quantity: item.quantity,
            expiryDate: new Date(item.expiry_date).toISOString()
        };

        // check if PO has a line item 
        if (initData[documentId].length === 0) {
            // add first line item to the existing PO
            lineItemData.lotNumbers.push(lotNumber);
            initData[documentId].push(lineItemData);
        } else {
            // check if line item already exists by looking at the previous line item 
            if (previousLineItem === lineItemData.lineItemId) {
                // iterate over each line item in the existing data and add new lot number
                initData[documentId].map((lineItem) => {
                    if (lineItem.lineItemId === lineItemData.lineItemId) {
                        // SUM = (current line item quantity + new lot number quantity) 
                        lineItem.quantity += lotNumber.quantity;
                        // Add new lot number item
                        lineItem.lotNumbers.push(lotNumber);
                    };
                    return true;
                });
            } else {
                // add a new line item to the existing PO
                lineItemData.lotNumbers.push(lotNumber);
                initData[documentId].push(lineItemData);
            };
        };

         // SET this line item id as previous line item
         previousLineItem = lineItemData.lineItemId;

        return true;     
    });

    // return final mapped data
    return initData;
};

export default DataTransformation;