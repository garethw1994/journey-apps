import React from 'react'
import { withRouter } from 'react-router-dom';
import { Row, Col } from 'antd';
// import util from '../../resource/utilities';
import Logo from '../../assets/ja-logo-footer.png';
import './Header.css';

const Header = () => {
  return (
    <div className="header">
      <Row>
          <Col span={6}></Col>
          <Col span={12} style={{ display: "flex", justifyContent: "center" }}>
          <img src={Logo} width="245px" height="60px" alt={<h5 className="app-heading">Journey Apps</h5>}></img>
        </Col>
        <Col span={6}></Col>
      </Row>
              
    </div>
  )
};

export default withRouter(Header);