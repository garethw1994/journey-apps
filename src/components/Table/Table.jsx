import React from 'react'
import { Row, Table, Button, Divider, Tag } from 'antd';

const DisplayTable = ({ items, submission }) => {
    /**
         * BuildTableColumns is a method for building table columns object
         * @argument {items} 
            * all the PO items    
         * @argument {process}
            * set to 'PO' when building documentid columns  
         * @ returns unique object that table renderer can process 
    */
    const BuildTableColumns = (items, process) => {
      if (process === 'PO') {
      // Build document id columns
        return [{
            title: 'PO Number',
            dataIndex: "documentid",
            key: 'documentid'
        }, {
            title: 'Submit PO Item',
            dataIndex: '',
            key: 'x',
            // Build each Button next to document id
            // Submit Button passes document id as arg to submission callback func
            render: (record) => <Button style={{ color: "#ffff", background: "#FF6C2F" }} onClick={() => submission(record)}> Submit PO </Button>
        }]
      } else {
          // Build columns for each key in items object
          let keys = Object.keys(items[0]);
          return keys.map((key) => {
            if (key !== 'lotNumbers') {
                return {
                    title: key,
                    dataIndex: key,
                    key
                }
            };
            // Return empty object if key is lotNumbers
            return {};
          })
      }
    };

     /**
         * BuildPOData is a method for building the data for PO table
         * @argument {items} 
            * all the PO items 
         * @returns { documentid: id } mapped object   
    */
    const BuildPOData = (items) => {
        let ids = Object.keys(items);
        return ids.map((id) => {
            return { documentid: id }
        })
    };

    /**
         * BuildLineItemTable is a method for building a Line Item Table
         * @argument {record} 
            * current PO/documentid item
         * @argument {items} 
            * all PO items
        * @returns {Table}  
    */
    const BuildLineItemTable = (record, items) => {
        return <Table expandedRowRender={record => BuildLotNumberTable(record)} columns={BuildTableColumns(items[record.documentid])} dataSource={items[record.documentid]}/>
    };

   /**
         * BuildLotNumberTable is a method for building a Lot Number Table
         * @argument {record} 
            * current line item
        * @returns {Table}  
    */
    const BuildLotNumberTable = (record) => {
        return <Table columns={BuildTableColumns(record.lotNumbers)} dataSource={record.lotNumbers}/>
    };

    return (
        <Row>
            <Row>
                <Divider><Tag color="#FF6C2F">PO Items</Tag></Divider>
            </Row>

            <Row>
                <Table
                expandedRowRender={ record => BuildLineItemTable(record, items)}    
                columns={BuildTableColumns(items, 'PO')} 
                dataSource={BuildPOData(items)} />
            </Row>
        </Row>
    )
  };

export default DisplayTable;
