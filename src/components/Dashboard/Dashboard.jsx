import React, { Component } from 'react'
import Header from '../Header/Header';
import dataTransformation from '../../dataFunctions/dataTransformation';
import { message, Icon, Row } from 'antd';
import Table from '../Table/Table';
import API from '../../resource/API';

export default class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lineItems: [],
      process: 'fetching',
      submissionToken: "Gareth1"
    }
  };

  componentWillMount = () => {
    // GET LINE ITEMS
    API('line_items', 'GET')
     .then((result) => {
         if (result) {
           // Pass result to Data Transform func to transform data into desired POST data
           dataTransformation(result)
            .then((items) => {
              // UPDATE COMPONENT STATE - set process to finish
              this.setState({
                lineItems: items,
                process: 'finished'
              })
            })
            .catch((error) => {
              // HANDLE ERROR
              console.log(error)
            })
         }
     })
     .catch((error) => {
       // HANDLE ERROR
       this.setState({
         process: 'error'
       })
     })
  };

  handleLineItemSubmission = (record) => {
    // all line items
    let allItems = this.state.lineItems;

    // current PO record to submit
    let PO = {};
    PO[record.documentid] = allItems[record.documentid];

    // My submission token
    let submissionToken = this.state.submissionToken;

    // POST DATA
    API('purchase_order', 'POST', PO,  submissionToken)
      .then((response) => {
        if (response.result === 'OK') {
          // remove item from PO list
          Object.keys(allItems).map((key) => {
            if (key === record.documentid) {
              delete allItems[key];
            };
            return true;
          });

          // show success message
          message.success(`Successfully Submitted PO: ${record.documentid}`);

          // update state
          this.setState({
            lineItems: allItems
          })
        };
      })
      .catch((error) => {
        // Hanlde Error
        console.log(error)
      })
  };

  render() {
    return (
      <div>
        <Header />
       {
         this.state.process === 'fetching' ?
         <Row style={{ display: 'grid', justifyContent: 'center' }}>
            <span style={{ display: 'flex', justifyContent: 'center', fontSize: '50px' }}><Icon type="loading"/></span>          
            <h4>Fetching Line Items...</h4>
         </Row>
         : 
         <Table items={this.state.lineItems} submission={this.handleLineItemSubmission}/>
       }
      </div>
    )
  }
}
