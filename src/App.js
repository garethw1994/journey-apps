import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Route, Switch, Redirect } from "react-router-dom";
import Dashboard from './components/Dashboard/Dashboard';

class App extends Component {
  render() {
    return (
      <div className="App" style={{
        height: "93vh"
      }}>
        <Switch>
          <Route path="/dashboard" component={Dashboard} />

          <Redirect from="/" exact to="/dashboard" />
        </Switch>
      </div>
    );
  }
}

export default App;
