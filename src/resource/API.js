/**
         * API() is a method to POST/GET data to/from a backend-service
            * @argument {endpoint} 
                * endpoint to a backend-service    
            * @argument {method}
                * type of method the request is e.g POST/GET
            * @argument {data} 
                * request data if it's a POST method   
            * @argument {submissionToken}
                * unique submission token  
         * @returns {Promise}
*/

import $ from 'jquery';

const API = (endpoint, method, data, submissionToken ) => {
    let baseURl = "https://api-interview-challenge.poweredbyjourney.com/"
    let url = `${baseURl}${endpoint}`;
    let auth = "Bearer yqj4hzd8v0e7xj3v8xkcqi";
    
    switch (method) {
        case "GET":
            return new Promise((resolve, reject) => {
                return $.ajax({
                    url,
                    method,
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": auth
                    }
                })
                .done((response) => {
                    resolve(response);
                })
                .fail((error) => {
                    reject(error)
                })
            })
        case "POST":
            return new Promise((resolve, reject) => {
                return $.ajax({
                        url,
                        method,
                        timeout: 0,
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": auth,
                            "Submission": submissionToken
                        },
                        data: JSON.stringify(data)
                    })
                    .done((response) => {
                        resolve(response);
                    })
                    .fail((error) => {
                        reject(error)
                    })
            });
        default:
            break;
    }
};

export default API;